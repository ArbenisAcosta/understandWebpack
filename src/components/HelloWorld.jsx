import React, { Component } from 'react';

export class HelloWorld extends Component {
  render() {
    return (
      <>
        <h1>¡Hola Mundo, Webpack con {this.props.name}!</h1>
        <img className="icon" src={this.props.logo} alt={this.props.name}></img>
        <nav className="menu">
          {this.props.menu.map((element) => (
            <a key={element[0]} href={element[1]}>
              {element[0]}
            </a>
          ))}
        </nav>
        {console.log(this.props)}
      </>
    );
  }
}
