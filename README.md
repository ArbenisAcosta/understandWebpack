# Webapack

[Curso de Webpack - jonmircha](https://www.youtube.com/watch?v=-bp3q-YTr4Q&list=PLvq-jIkSeTUaPCCQLLE9d2E-LbkG6ax4i&index=1)

---

INDICE:

1. Bienvenida
2. Introducción a Webpack
3. Documentación
4. Instalando Webpack y su CLI
5. Webpack sin configuración
6. Modos de Webpack
7. Diferentes puntos de entrada y salida desde la CLI
8. Transpilando JS con Babel
9. Inyección de JS en HTML
10. Extracción de CSS
11. Servidor Web de Desarrollo
12. Manejo de archivos
13. Haciendo desarrollo web con Webpack
14. Manejo de urls en CSS
15. Optimizando Imágenes
16. Manejo de JSON
17. Múltiples puntos de entrada y salida
18. Componente Hola Mundo Vanilla JS
19. Componente Hola Mundo TypeScript
20. Componente Hola Mundo React
21. Despedida
